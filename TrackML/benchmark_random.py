### https://www.kaggle.com/jbonatt/random-benchmark/

import pandas as pd
import numpy as np

from trackml.dataset import load_event, load_dataset
from trackml.randomize import shuffle_hits
from trackml.score import score_event

#event_id = 'event000001000'
#hits, cells, particles, truth = load_event('data/train_100_events/'+event_id)


# https://sites.google.com/site/trackmlparticle/
''' For each collision, about 10.000 space tracks (helicoidal trajectories 
originating approximately from the center of the detector), will leave about 
10 precise 3D points. The core pattern recognition tracking task is to 
associate the 100.000 3D points into tracks.'''

submission = pd.DataFrame()#columns=['event_id','hit_id','track_id'])

for event_id, hits, cells in load_dataset('data/test/', parts=['hits', 'cells']):

  avg = 10
  nhits = len(hits)
  d, r = divmod(nhits, avg)
  tracks_d = np.tile(range(d), avg)
  tracks_r = np.array(range(r))
  tracks = np.concatenate([tracks_d, tracks_r ])

  col_event_id = [event_id] * nhits
  col_hits_id = hits.hit_id.values
  col_track_id = tracks
  
  event = pd.DataFrame()
  event['event_id'] = col_event_id
  event['hit_id'] = col_hits_id
  event['track_id'] = col_track_id

  submission = pd.concat([submission,event], axis=0)

  s = 'event_id={}, nhits={}, r={}, d={}, len(tracks)={}, len(submission)={}'
  print(s.format(event_id, nhits, d, r, len(tracks), len(submission)))

# track_id changes from int to float during pd.concat. Change back.
submission = submission.astype('int32')

#submission.to_csv('benchmark_random.csv.zip', index=False, compression='zip')
submission.to_csv('benchmark_random.csv', index=False)

