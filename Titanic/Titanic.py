
### Titanic: Machine Learning from Disaster
### https://www.kaggle.com/c/titanic

### Competition Description
'''
The sinking of the RMS Titanic is one of the most infamous shipwrecks in 
history.  On April 15, 1912, during her maiden voyage, the Titanic sank after 
colliding with an iceberg, killing 1502 out of 2224 passengers and crew. This 
sensational tragedy shocked the international community and led to better 
safety regulations for ships.

One of the reasons that the shipwreck led to such loss of life was that there 
were not enough lifeboats for the passengers and crew. Although there was some 
element of luck involved in surviving the sinking, some groups of people were 
more likely to survive than others, such as women, children, and the 
upper-class.

In this challenge, we ask you to complete the analysis of what sorts of people 
were likely to survive. In particular, we ask you to apply the tools of machine 
learning to predict which passengers survived the tragedy.
'''

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import scale
from sklearn.model_selection import train_test_split


### Import dataset
train = pd.read_csv('Data/train.csv')
test = pd.read_csv('Data/test.csv')

### Copy original df for reference post-droppings
train0 = train.copy()
test0 = test.copy()

### Get column/header names. May be useful later
train_columns = train.columns.values

### Print some diagnostics
'''
print()
print(train_columns)
print()
print(train.info())
print()
print(train.describe(include='all'))
'''


##### PREPROCESSING

### Drop immediately (assumed and/or potentially) useless information
# PassengerId is just row+1, and
# Ticket has some prefixes but don't want to work with them currently
#  Don't drop test Ticket because we need that feature for submission
# Cabin is mostly NaN. It can't be that important, can it? 
#  Would otherwise sound important. May try to reclaim this later.
#  SEE FARTHER DOWN FOR CORRECTION: Is important.
train = train.drop(['PassengerId', 'Ticket'], axis=1)
test = test.drop(['Ticket'], axis=1)

### Sum SibSp and Parch to create Family feature
train['Family'] = train['SibSp'] + train['Parch']
test['Family'] = test['SibSp'] + test['Parch']
# Now drop SibSp and Parch to declutter
train = train.drop(['SibSp', 'Parch'], axis=1)
test = test.drop(['SibSp', 'Parch'], axis=1)

### Pull titles from Name
# Names themselves may be useless but what title that person held may be useful
# All titles are immediately suceeded by a ".", e.g., "Mr."
# Find all (+) alphabet characters (a-zA-Z) preceeded by " " and preceeding "."
# RegEx from https://www.kaggle.com/startupsci/titanic-data-science-solutions
train['Title'] = train['Name'].str.extract(' ([a-zA-Z]+)\.', expand=False)
test['Title'] = test['Name'].str.extract(' ([a-zA-Z]+)\.', expand=False)
# Combine different names for the same title (English vs French)
#train['Title'].value_counts()
fr = ['Mlle', 'Ms',   'Mme']
eng = ['Miss', 'Miss', 'Mrs']
train['Title'] = train['Title'].replace(fr, eng)
test['Title'] = test['Title'].replace(fr, eng)
# Replace all others into Special title. Dona is solely in test
other = ['Dr','Rev','Col','Major','Jonkheer','Lady','Capt','Don','Sir','Countess','Dona']
special = ['Special']*len(other)
train['Title'] = train['Title'].replace(other, special)
test['Title'] = train['Title'].replace(other, special)
# Title can now be categorized into tier
# Now drop Name to declutter
train = train.drop('Name', axis=1)
test = test.drop('Name', axis=1)

##### Which columns still have nans?
# From https://stackoverflow.com/questions/14247586/python-pandas-how-to-select-rows-with-one-or-more-nulls-from-a-dataframe-without
nans = lambda df: df[df.isnull().any(axis=1)]
#print(nans(train))
# It seems many Age, and 2 Embarked, have nans for both train and test

### Replace nan Ages with average Age of for their Title
titles = train.groupby('Title')['Age'].mean().index
ages = train.groupby('Title')['Age'].mean().values
for t,a in zip(titles,ages): 
  # Set row in column Age of df train/test with (Age=nan and Title=t) equal to a
  train.loc[ train['Age'].isnull() & (train['Title'] == t), 'Age' ] = a
  test.loc[ test['Age'].isnull() & (test['Title'] == t), 'Age' ] = a

### Replace nan Embarks with most common
maxEmbarked = max(train['Embarked'].value_counts().index)
#train.loc[train['Embarked'].isnull()] = maxEmbarked
#test.loc[train['Embarked'].isnull()] = maxEmbarked
train['Embarked'] = train['Embarked'].fillna(maxEmbarked)
test['Embarked'] = test['Embarked'].fillna(maxEmbarked)

### Check if any nans STILL exist:
#  nans(train) or train.isnull().any() == good
#  nans(test) or test.isnull().any() == Fare still has one nan
# Make it mean or median of that Pclass
### Replace nan Fares with mean/median of that persons Pclass
pclass = nans(test)['Pclass'].values[0]
meanFare = train[train['Pclass'] == pclass]['Fare'].median()# mean()
test.loc[test['Fare'].isnull(), 'Fare'] = meanFare


### Convert Sex={female,male} to Sex={0,1}
train['Sex'] = train['Sex'].replace(['female','male'], [0,1])
test['Sex'] = test['Sex'].replace(['female','male'], [0,1])


### Convert Titles to integers, too
titles = train['Title'].value_counts().index
# = Index(['Mr', 'Miss', 'Mrs', 'Master', 'Special'], dtype='object')
titlenums = range(len(titles))
# = range(0,5)
train['Title'] = train['Title'].replace(titles, titlenums)
test['Title'] = test['Title'].replace(titles, titlenums)


### Convert Embarked to integers, too
embarked =  train['Embarked'].value_counts().index
# = Index(['S', 'C', 'Q'], dtype='object')
embarkednums = range(len(embarked))
# = range(0,3)
train['Embarked'] = train['Embarked'].replace(embarked, embarkednums)
test['Embarked'] = test['Embarked'].replace(embarked, embarkednums)



### Do some (more) feature engineering
# Got these ideas from 
# https://www.kaggle.com/ldfreeman3/a-data-science-framework-to-achieve-99-accuracy
# Is the passenger alone? I.e., is Family zero?
train['Alone'] = 1 # Initialize
train['Alone'].loc[train['Family'] >= 1] = 0


### It actually turns out that Cabin is pretty powerful:
#sum(train0['Cabin'].isnull()) = 687
#train0['Cabin'].isnull().mean() = 0.77
# So about 1/3 have no cabin details
#train0.loc[ train0['Survived'] == 0 & train0['Cabin'].isnull()] = 549
# 549 of those 687 died. Looks like a good predictor of death to me.
### Make feature that tells whether a passenger had a cabin on the Titanic.
# Fill all nans with 'U'
train['Cabin'] = train['Cabin'].fillna('U') # U == Unknown
test['Cabin'] = test['Cabin'].fillna('U') 
# Cabin letter, drop numbers. Letters mean level or something?
train['Cabin'] = train['Cabin'].str.extract('([A-Z])', expand=False)
test['Cabin'] = test['Cabin'].str.extract('([A-Z])', expand=False)
# Make feature that tells whether a passenger had a cabin on the Titanic
cabins = train['Cabin'].value_counts().index[1:]
train['HasCabin'] = train['Cabin'].replace('U', 0)
train['HasCabin'] = train['HasCabin'].replace(cabins, 1)
test['HasCabin'] = test['Cabin'].replace('U', 0)
test['HasCabin'] = test['HasCabin'].replace(cabins, 1)

### Convert Cabin to integers, too
cabins = train['Cabin'].value_counts().index
# = Index(['U', 'C', 'B', 'D', 'E', 'A', 'F', 'G', 'T'], dtype='object')
cabinnums = range(len(cabins))
# = range(0,9)
train['Cabin'] = train['Cabin'].replace(cabins, cabinnums)
test['Cabin'] = test['Cabin'].replace(cabins, cabinnums)









### Plot age vs survival
#df0 = train[train['Survived']==0]['Age']/len(train[train['Survived']==0]['Age'])*100
#df1 = train[train['Survived']==1]['Age']/len(train[train['Survived']==1]['Age'])*100
'''
dfage = train.groupby('Age')['Survived'].mean()*100
f,ax = plt.subplots(1,3)
ax[0].set_title('Survived = 0')
train[train['Survived']==0]['Age'].plot.hist(ax=ax[0], bins=40, edgecolor='k')
#df0.plot.hist(ax=ax[0], bins=40, edgecolor='k')#, normed=True)
ax[1].set_title('Survived = 1')
train[train['Survived']==1]['Age'].plot.hist(ax=ax[1], bins=40, edgecolor='k')
#df1.plot.hist(ax=ax[1], bins=40, edgecolor='k')#, normed=True)
ax[2].set_title('Survived %')
dfage.plot.bar(ax=ax[2])
#import seaborn as sns
#sns.barplot(data=dfage)
plt.show(block=False)
'''





'''
import seaborn as sns
sns.set(style='ticks')
#topairplot = ['Pclass', 'Age', 'Fare', 'Family'] #list(train)[1:]
sns.pairplot(train, hue='Survived', size=0.8)#, vars=topairplot)
plt.show(block=False)
'''





### From the Competition Description, 1 - 1502/2224 = 32.5% survived.
#  "... some groups of people were more likely to survive than others, 
#  such as women, children, and the upper-class"
# --> Pivot Sex, Age, SibSp, Parch, Pclass 
#for i in np.delete(train_columns, list(train_columns).index('Survived')):
'''
for i in ['Sex', 'Age', 'Pclass', 'Family']:
  print()
  # Group i and Survived by i. Make i a column, not the index. Take mean of Survived wrt i. Sort Survived mean high to low.
  print(train[[i, 'Survived']].groupby([i], as_index=False).mean().sort_values(by='Survived', ascending=False))
'''







'''
train.groupby(['Sex','Pclass'])['Survived'].mean()
train.groupby(['Sex','Age'])['Survived'].mean()
train.groupby(['Sex','Fare'])['Survived'].mean()
train.groupby(['Sex','Embarked'])['Survived'].mean()
train.groupby(['Sex','Family'])['Survived'].mean()
train.groupby(['Sex','Title'])['Survived'].mean()
'''





'''
k = 10
kNN = KNeighborsClassifier(n_neighbors = k)
kNN.fit(X_train, y_train)
train_accuracy = kNN.score(X_train, y_train)
test_prediction = kNN.predict(test)

df = pd.DataFrame(data=test_prediction, index=test['PassengerId'], columns=['Survived'])
df.to_csv('gender_submission.csv', header=True)

#
'''



### Relabel variables for easier reading
X_train = train.drop(['Survived'],axis=1)
y_train = train['Survived']
#X_test = test



### Split train data into train and test to check kNN accuracy.
# Otherwise no way to in situ see determine k and accuracy
split = 2/3.
X_train, X_test, y_train, y_test = \
  train_test_split(X_train, y_train, test_size=1-split, stratify=y_train)

### Scale data...
X_train = scale(X_train)
X_test = scale(X_test)

### Do kNN for some range of k.
# From https://campus.datacamp.com/courses/supervised-learning-with-scikit-learn/classification?ex=12
# and from sklearn/kNN.py
def kNN_range(X_train, y_train, X_test, y_test, ki=1, kf=19):
    # Setup arrays to store train and test accuracies
    neighbors = np.arange(ki, kf)
    train_accuracy = np.empty(len(neighbors))
    test_accuracy = np.empty(len(neighbors))

    # Loop over different values of k
    for i, k in enumerate(neighbors):
        # Setup a k-NN Classifier with k neighbors: knn
        knn = KNeighborsClassifier(n_neighbors=k)#, weights='distance')
        # Fit the classifier to the training data
        knn.fit(X_train, y_train)

        #Compute accuracy on the training set
        train_accuracy[i] = knn.score(X_train, y_train)

        #Compute accuracy on the testing set
        test_accuracy[i] = knn.score(X_test, y_test)

    return neighbors, train_accuracy, test_accuracy


ki = 1
kf = int(np.sqrt(len(train)+len(test))) # = 36
### Do kNN once
'''
neighbors, train_accuracy, test_accuracy = \
  kNN_range(X_train, y_train, X_test, y_test, ki=ki, kf=kf)

# Generate plot
plt.figure()
plt.title('kNN: Titanic')
plt.plot(neighbors, train_accuracy, label = 'Training')
plt.plot(neighbors, test_accuracy, label = 'Test')
plt.legend()
plt.xlabel('k')
plt.ylabel('Accuracy')
#plt.savefig(toydataset+'/kNN_'+toydataset+'#_acc.png')
plt.show(block=False)
'''


### Do kNN range for some iterations to smooth out k vs accuracy. This includes resplitting data
# This is kind of like kFolds cross validation but all instead of parts
# I could then do THIS in a loop to find optimal iters... but I haven't yet.
kVsAcc = []
iters = kf*10 #20 #500
for i in range(iters):
    X_tr, X_te, y_tr, y_te = train_test_split(X_train, y_train, test_size=1-split, stratify=y_train)
    n, tr_a, te_a = kNN_range(X_tr, y_tr, X_te, y_te, ki=ki, kf=kf)
    kVsAcc.append( (tr_a, te_a) )

for i in kVsAcc: i[1][0]


### Difference between min/max k. Some ~easy way to see volitility in k.
# Lower diff = bestter
kVsAcc_train_min = np.max(kVsAcc,axis=0)[0]
kVsAcc_train_max = np.min(kVsAcc,axis=0)[0]
kVsAcc_test_min = np.max(kVsAcc,axis=0)[1]
kVsAcc_test_max = np.min(kVsAcc,axis=0)[1]
'''
#diffk = np.max(kVsAcc, axis=0)-np.min(kVsAcc, axis=0)
diffk_train = kVsAcc_train_max - kVsAcc_train_min #diffk[0]
diffk_test = kVsAcc_test_max - kVsAcc_test_max #diffk[1]
plt.figure()
plt.title('Max-Min k (iterations='+str(iters)+'): Titanic')
plt.plot(n, diffk_train, label='Train')
plt.plot(n, diffk_test, label='Test')
plt.legend()
plt.xlabel('k')
plt.ylabel('max(k) - min(k)')
plt.show(block=False)
'''


### Plot mean k vs accuracy
kVsAcc_mean = np.mean(kVsAcc, axis=0)
Train_accuracy = kVsAcc_mean[0]
Test_accuracy = kVsAcc_mean[1]

# Generate plot with diffs as "error bands"
plt.figure()
plt.title('Mean kNN (iterations='+str(iters)+'): Titanic')
plt.plot(n, kVsAcc_train_max, color='orange', linewidth=1, linestyle=':')
plt.plot(n, Train_accuracy, label = 'Training', color='orange')
plt.plot(n, kVsAcc_train_min, color='orange', linewidth=1, linestyle=':')
plt.plot(n, kVsAcc_test_max, color='blue', linewidth=1, linestyle=':')
plt.plot(n, Test_accuracy, label = 'Test', color='blue')
plt.plot(n, kVsAcc_test_min, color='blue', linewidth=1, linestyle=':')
plt.legend()
plt.xlabel('k')
plt.ylabel('Accuracy')
plt.savefig('MeankNN.png')
plt.show(block=False)


### Do final kNN with above-found best k on real test data
# Relabel variables for easier reading and to get bigger data again
X_train = train.drop(['Survived'],axis=1)
y_train = train['Survived']
# With some k nearest neighbors, create classifier
k = Test_accuracy.argmax()+1 #test_accuracy.argmax()+1 #6
print('')
print('Optimal k =', k)
kNN = KNeighborsClassifier(n_neighbors=k)#, weights='distance')
# "Fit" the classifier to the data
kNN.fit(X_train, y_train)

### Pass test data (or any data that meets type/form) and have the now-fitted classifier predict targets
prediction = kNN.predict(test.drop('PassengerId', axis=1))#test)
prediction_probs = kNN.predict_proba(test.drop('PassengerId', axis=1))#test)
print(prediction)

### Save prediction to csv for submission
df = pd.DataFrame(data=prediction, index=test['PassengerId'], columns=['Survived'])
df.to_csv('gender_submission.csv', header=True)
