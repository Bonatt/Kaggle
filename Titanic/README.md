### Kaggle's "[Titanic: Machine Learning from Disaster](https://www.kaggle.com/c/titanic)"

>>>
The sinking of the RMS Titanic is one of the most infamous shipwrecks in
history.  On April 15, 1912, during her maiden voyage, the Titanic sank after
colliding with an iceberg, killing 1502 out of 2224 passengers and crew. This
sensational tragedy shocked the international community and led to better
safety regulations for ships.

One of the reasons that the shipwreck led to such loss of life was that there
were not enough lifeboats for the passengers and crew. Although there was some
element of luck involved in surviving the sinking, some groups of people were
more likely to survive than others, such as women, children, and the
upper-class.

In this challenge, we ask you to complete the analysis of what sorts of people
were likely to survive. In particular, we ask you to apply the tools of machine
learning to predict which passengers survived the tragedy.
>>>

Please see my [Jupyter notebook](https://gitlab.com/Bonatt/Kaggle/blob/master/Titanic/Titanic.ipynb).

<!--
I thought about doing this write-up in an IPython notebook but decided against it. 

I'll first go through my preprocessing methods and thoughts (which I believe are model independent).
Then I'll go through my attempts: 
my first attempt employed (surprise, surprise) a dumb kNN algorithm;
my second attempt employed a random forest.

### Preprocessing

```python
train = pd.read_csv('Data/train.csv')
test = pd.read_csv('Data/test.csv')
```

I imported the data above a pandas DataFrame and below looked at the form and some statistics:

```
train.head()

   PassengerId  Survived  Pclass  \
0            1         0       3
1            2         1       1
2            3         1       3
3            4         1       1
4            5         0       3

                                                Name     Sex   Age  SibSp  \
0                            Braund, Mr. Owen Harris    male  22.0      1
1  Cumings, Mrs. John Bradley (Florence Briggs Th...  female  38.0      1
2                             Heikkinen, Miss. Laina  female  26.0      0
3       Futrelle, Mrs. Jacques Heath (Lily May Peel)  female  35.0      1
4                           Allen, Mr. William Henry    male  35.0      0

   Parch            Ticket     Fare Cabin Embarked
0      0         A/5 21171   7.2500   NaN        S
1      0          PC 17599  71.2833   C85        C
2      0  STON/O2. 3101282   7.9250   NaN        S
3      0            113803  53.1000  C123        S
4      0            373450   8.0500   NaN        S
```

```
train.info()

<class 'pandas.core.frame.DataFrame'>
RangeIndex: 891 entries, 0 to 890
Data columns (total 12 columns):
PassengerId    891 non-null int64
Survived       891 non-null int64
Pclass         891 non-null int64
Name           891 non-null object
Sex            891 non-null object
Age            714 non-null float64
SibSp          891 non-null int64
Parch          891 non-null int64
Ticket         891 non-null object
Fare           891 non-null float64
Cabin          204 non-null object
Embarked       889 non-null object
dtypes: float64(2), int64(5), object(5)
memory usage: 83.6+ KB
```

```
train.describe(include='all')

        PassengerId    Survived      Pclass  \
count    891.000000  891.000000  891.000000
unique          NaN         NaN         NaN
top             NaN         NaN         NaN
freq            NaN         NaN         NaN
mean     446.000000    0.383838    2.308642
std      257.353842    0.486592    0.836071
min        1.000000    0.000000    1.000000
25%      223.500000    0.000000    2.000000
50%      446.000000    0.000000    3.000000
75%      668.500000    1.000000    3.000000
max      891.000000    1.000000    3.000000

                                                Name   Sex         Age  \
count                                            891   891  714.000000
unique                                           891     2         NaN
top     Duff Gordon, Sir. Cosmo Edmund ("Mr Morgan")  male         NaN
freq                                               1   577         NaN
mean                                             NaN   NaN   29.699118
std                                              NaN   NaN   14.526497
min                                              NaN   NaN    0.420000
25%                                              NaN   NaN   20.125000
50%                                              NaN   NaN   28.000000
75%                                              NaN   NaN   38.000000
max                                              NaN   NaN   80.000000

             SibSp       Parch Ticket        Fare    Cabin Embarked
count   891.000000  891.000000    891  891.000000      204      889
unique         NaN         NaN    681         NaN      147        3
top            NaN         NaN   1601         NaN  B96 B98        S
freq           NaN         NaN      7         NaN        4      644
mean      0.523008    0.381594    NaN   32.204208      NaN      NaN
std       1.102743    0.806057    NaN   49.693429      NaN      NaN
min       0.000000    0.000000    NaN    0.000000      NaN      NaN
25%       0.000000    0.000000    NaN    7.910400      NaN      NaN
50%       0.000000    0.000000    NaN   14.454200      NaN      NaN
75%       1.000000    0.000000    NaN   31.000000      NaN      NaN
max       8.000000    6.000000    NaN  512.329200      NaN      NaN

```
-->

