# [Kaggle](https://www.kaggle.com/)

> Kaggle is a platform for predictive modelling and analytics competitions in which statisticians and data miners compete to produce the best models for predicting and describing the datasets uploaded by companies and users.

Most of my Kaggling is done privately, but I do sometimes participate with the community. [Here is my account](https://www.kaggle.com/jbonatt). 
